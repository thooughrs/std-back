from rest_framework import serializers, status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from users.models import User


@api_view(['POST'])
@permission_classes((AllowAny,))
def user_register(request):
    user = User.objects.filter(username=request.data['username'])
    if user:
        return Response({'message': 'User with this email or username already exists'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        user = User(
            username=request.data['username'],
        )
        user.set_password(request.data['password'])
        user.save()
        token, _ = Token.objects.get_or_create(user=user)
        return Response({"token": token.key}, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def user_login(request):
    user = User.objects.filter(username=request.data['username']).first()
    if user:
        if user.check_password(request.data['password']):
            token, _ = Token.objects.get_or_create(user=user)
            return Response({"token": token.key}, status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Password is incorrect'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response({'message': 'User not found'}, status=status.HTTP_400_BAD_REQUEST)